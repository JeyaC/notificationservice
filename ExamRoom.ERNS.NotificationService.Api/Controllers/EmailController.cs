using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ExamRoom.ERNS.NotificationService.Api.Controllers
{
    [Route("api/[controller]")]
    public class EmailController : ControllerBase
    {
        private readonly ILogger<EmailController> logger;
        private readonly INotificationService notificationService;

        public EmailController(ILogger<EmailController> logger,
           INotificationService notificationService)
        {
            this.logger = logger;
            this.notificationService = notificationService;
        }

        [HttpPost]
        [Route("SendEmail")]
        public async Task<IActionResult> SendEmail([FromBody] SendEmailModel sendEmailModel)
        {
            try
            {
                logger.LogInformation($"ERNS SendEmail - start");
                var result = await notificationService.SendEmail(sendEmailModel);
                logger.LogInformation($"ERNS SendEmail Result - {result}");
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.LogError($"SendEmail Failed: {ex.Message}");
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
        [HttpPost]
        [Route("SendNotification")]
        public async Task<IActionResult> SendNotification([FromBody] SendEmailModel sendEmailModel)
        {
            try
            {
                logger.LogInformation($"ERNS SendEmail - start");
                var result = await notificationService.SendEmail(sendEmailModel);
                logger.LogInformation($"ERNS SendEmail Result - {result}");
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.LogError($"SendEmail Failed: {ex.Message}");
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
