﻿using ExamRoom.ERNS.NotificationService.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace ExamRoom.ERNS.NotificationService.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly ILogger<TestController> logger;
        private readonly ERNotificationQueue notificationQueue;
        private readonly INotificationService notificationService;

        public TestController(ILogger<TestController> logger, 
            ERNotificationQueue notificationQueue,
            INotificationService notificationService)
        {
            this.logger = logger;
            this.notificationQueue = notificationQueue;
            this.notificationService = notificationService;
        }
        [HttpPost]
        public async Task<string> Listen()
        {
            while (true)
            {
                try
                {
                    var message = await notificationQueue.Get();
                    if (message != null)
                    {
                        logger.LogInformation($"Message recieved - {message.Body}");
                        var sqsMessage = JsonConvert.DeserializeObject<SendEmailSMTPModel>(message?.Body);

                        await notificationService.SendSMTPEmail(sqsMessage);

                        await notificationQueue
                               .Delete(message);
                    }
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, "Exception");
                }
            }
        }
    }
}
