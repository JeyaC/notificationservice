//using System;
//using System.Collections.Generic;
//using System.Net;
//using System.Threading.Tasks;
//using Amazon.Lambda.APIGatewayEvents;
//using Amazon.Lambda.Core;
//using Amazon.Lambda.SQSEvents;
//using ExamRoom.ERNS.NotificationService.Core;
//using Newtonsoft.Json;


//// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
//[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

//namespace ExamRoom.ERNS.NotificationService.Lambda
//{
//    public class Function
//    {
//        readonly AppServices services;
//        readonly NotificationService notificationService;
//        /// <summary>
//        /// Default constructor. This constructor is used by Lambda to construct the instance. When invoked in a Lambda environment
//        /// the AWS credentials will come from the IAM role associated with the function and the AWS region will be set to the
//        /// region the Lambda function is executed in.
//        /// </summary>
//        public Function()
//        {
//            var settings = Bootstrapper
//                  .GetSettings();
//            services = settings.GetServices();
//            notificationService = new NotificationService(settings, services);
//        }

//        /// <summary>
//        /// This method is called for every Lambda invocation. This method takes in an SQS event object and can be used 
//        /// to respond to SQS messages.
//        /// </summary>
//        /// <param name="evnt"></param>
//        /// <param name="context"></param>
//        /// <returns></returns>
//        public async Task FunctionHandler(SQSEvent evnt, ILambdaContext context)
//        {
//            foreach (var message in evnt.Records)
//            {
//                await ProcessMessageAsync(message);
//            }
//        }

//        public async Task<APIGatewayProxyResponse> SendEmailHandler(APIGatewayProxyRequest request, ILambdaContext context)
//        {
//            var message = JsonConvert.DeserializeObject<SendEmailModel>(request?.Body);
//            try
//            {
//                context.Logger.LogLine($"SendEmail - Begin");
//                await notificationService.SendEmail(message);
//                context.Logger.LogLine($"SendEmail - Sucess");
//            }
//            catch (Exception ex)
//            {
//                context.Logger.LogLine($"SendEmail Failed: {ex.Message}");
//                return new APIGatewayProxyResponse
//                {
//                    StatusCode = (int)HttpStatusCode.BadRequest,
//                    Body = ex.Message,
//                    Headers = new Dictionary<string, string> { { "Content-Type", "text/plain" } }
//                };
//            }
//            return new APIGatewayProxyResponse
//            {
//                StatusCode = (int)HttpStatusCode.OK,
//                Body = "Sucess",
//                Headers = new Dictionary<string, string> { { "Content-Type", "text/plain" } }
//            };
//        }

//        //public async SendTextHandler()
//        //{
//        //    var snsClient = new AmazonSimpleNotificationServiceClient(
//        //        snsAccessKeyId,
//        //        snsSecretKey,
//        //        RegionEndpoint.GetBySystemName(endpointName));
//        //}

//        //public async Task Notify(SendEmailModel msg)
//        //{
//        //    var msgString = JsonConvert.SerializeObject(msg);
//        //    var publishRequest = new PublishRequest(snsArn, msgString);

//        //    await snsClient
//        //        .PublishAsync(publishRequest);
//        //}

//        private async Task ProcessMessageAsync(SQSEvent.SQSMessage message)
//        {
//            var sqsMessage = JsonConvert.DeserializeObject<SendEmailModel>(message?.Body);
//            await notificationService.SendSMTPEmail(sqsMessage);
//            await Task.CompletedTask;
//        }
//    }
//}