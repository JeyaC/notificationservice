﻿namespace ExamRoom.ERNS.NotificationService.Core
{
    public class AppSettings
    {
        public AwsCreds AwsCreds { get; set; }
        public NotificationQueue ERNotificationQueue { get; set; }
        public EmailInfo EmailInfo { get; set; }
    }
    public class AwsCreds
    {
        public string AccessKeyId { get; set; }
        public string SecretKey { get; set; }
        public string RegionName { get; set; }
    }

    public class NotificationQueue
    {
        public string Url { get; set; }
    }

    public class EmailInfo
    {
        public string UserName { get; set; }
        public string Bcc { get; set; }
        public string Password { get; set; }
        public bool EnableSsl { get; set; }
        public string Port { get; set; }
        public string Host { get; set; }
        public string SmtpUserName { get; set; }
        public string SmtpPassword { get; set; }
    }
}
