﻿using Amazon;
using Amazon.Extensions.NETCore.Setup;
using Amazon.S3;
using Amazon.SimpleEmail;
using Amazon.SimpleNotificationService;
using Amazon.SQS;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ExamRoom.ERNS.NotificationService.Core
{
    public static class Bootstrapper
    {
        public static IServiceCollection AddComponents(this IServiceCollection service, IConfiguration configuration)
        {
            service
                .AddServices(configuration);
            return service;
        }

        public static IServiceCollection AddServices(this IServiceCollection service, IConfiguration configuration)
        {
            var options = configuration.GetAWSOptions("Appsettings:AwsCreds");

            service
                .AddDefaultAWSOptions(GetAwsOptions(configuration));
            service
                .AddAWSService<IAmazonS3>();
            service
                .AddAWSService<IAmazonSQS>();
            service
                .AddAWSService<IAmazonSimpleNotificationService>();
            service
                .AddAWSService<IAmazonSimpleEmailService>();
            service
                .Configure<AppSettings>(configuration.GetSection("AppSettings"));
            service
                .AddScoped<ERNotificationQueue>();
            service
                .AddScoped<INotificationService, NotificationService>();

            return service;
        }

        private static AWSOptions GetAwsOptions(IConfiguration configuration)
        {
            return new AWSOptions()
            {
                //Credentials = new BasicAWSCredentials(configuration["Appsettings:AwsCreds:AccessKeyId"], configuration["Appsettings:AwsCreds:SecretKey"]),
                Region = RegionEndpoint.GetBySystemName(configuration["AwsCreds:RegionName"])
            };
        }
    }
}
