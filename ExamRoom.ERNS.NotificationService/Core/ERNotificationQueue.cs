﻿using Amazon.SQS;
using Amazon.SQS.Model;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Threading.Tasks;
using Message = Amazon.SQS.Model.Message;

namespace ExamRoom.ERNS.NotificationService.Core
{
    public class ERNotificationQueue
    {
        private readonly ILogger<ERNotificationQueue> logger;
        private IAmazonSQS client;
        private string queueUrl;
        public ERNotificationQueue(ILogger<ERNotificationQueue> logger, IOptions<AppSettings> options, IAmazonSQS sqsClient)
        {
            this.logger = logger;
            this.client = sqsClient;
            this.queueUrl = options.Value.ERNotificationQueue.Url;
        }
        public async Task<Message> Get()
        {
            var request = new ReceiveMessageRequest
            {
                WaitTimeSeconds = 20,
                MaxNumberOfMessages = 1,
                QueueUrl = queueUrl
            };
            var response = await client.ReceiveMessageAsync(request);
            return response.Messages.FirstOrDefault();
        }
        public async Task Delete(Message msg)
        {
            var deleteRequest = new DeleteMessageRequest
            {
                ReceiptHandle = msg.ReceiptHandle,
                QueueUrl = queueUrl
            };

            await client
                 .DeleteMessageAsync(deleteRequest);
        }
    }
}