﻿using Amazon;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using ExamRoom.ERNS.NotificationService.Core;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace ExamRoom.ERNS.NotificationService
{
    public interface INotificationService
    {
        Task<bool> SendEmail(SendEmailModel messageContent);
        Task<bool> SendSMTPEmail(SendEmailSMTPModel messageContent);
        Task<bool> SendNotification(SendEmailModel messageContent);
    }

    public class NotificationService : INotificationService
    {
        #region Private Members
        private readonly AppSettings settings;
        private readonly ILogger<NotificationService> logger;
        private readonly IAmazonSimpleEmailService amazonSimpleEmailService;

        #endregion

        public NotificationService(ILogger<NotificationService> logger, IOptions<AppSettings> settings, IAmazonSimpleEmailService amazonSimpleEmailService)
        {
            this.settings = settings.Value;
            this.logger = logger;
            this.amazonSimpleEmailService = amazonSimpleEmailService;
        }

        #region Public Methods
        public async Task<bool> SendEmail(SendEmailModel messageContent)
        {
            logger.LogInformation(JsonConvert.SerializeObject(messageContent));
            string senderAddress = settings?.EmailInfo?.UserName;
            string subject = messageContent?.Subject;
            string textBody = messageContent.TextBody;
            string htmlBody = messageContent?.HtmlBody;

            if (messageContent.BccmailIds == null) 
                messageContent.BccmailIds = new List<string> { settings?.EmailInfo?.Bcc }; // Better Implementation

            var sendRequest = new SendEmailRequest
            {
                Source = senderAddress,
                Destination = new Destination
                {
                    ToAddresses = messageContent.ToAddress,
                    CcAddresses = messageContent?.CcmailIds,
                    BccAddresses = messageContent?.BccmailIds
                },
                Message = new Message
                {
                    Subject = new Content(subject),
                    Body = new Body
                    {
                        Html = new Content
                        {
                            Charset = "UTF-8",
                            Data = htmlBody
                        }
                    }
                },
            };
            try
            {
                logger.LogInformation($"Sending email using Amazon SES...");
                var response = await amazonSimpleEmailService.SendEmailAsync(sendRequest);
                bool status = !string.IsNullOrEmpty(response?.MessageId) ? true : false;
                logger.LogInformation($"MessageID: {response?.MessageId}, Status: {status}");

                return status;
            }
            catch (Exception ex)
            {
                logger.LogError($"The email was not sent.");
                logger.LogError($"Error message: {ex.Message}");
                return false;
            }
        }
        public async Task<bool> SendNotification(SendEmailModel messageContent)
        {
            logger.LogInformation(JsonConvert.SerializeObject(messageContent));
            string senderAddress = string.IsNullOrEmpty(messageContent.SenderAddress) ?  settings?.EmailInfo?.UserName : messageContent.SenderAddress;
            string subject = messageContent?.Subject;
            string textBody = messageContent.TextBody;
            string htmlBody = messageContent?.HtmlBody;

            var sendRequest = new SendEmailRequest
            {
                Source = senderAddress,
                Destination = new Destination
                {
                    ToAddresses = messageContent.ToAddress.ToList(),
                    CcAddresses = messageContent.CcmailIds.ToList(),
                    BccAddresses = messageContent.BccmailIds.ToList()
                },
                Message = new Message
                {
                    Subject = new Content(subject),
                    Body = new Body
                    {
                        Html = new Content
                        {
                            Charset = "UTF-8",
                            Data = htmlBody
                        }
                    }
                },
            };
            try
            {
                logger.LogInformation($"Sending email using Amazon SES...");
                var response = await amazonSimpleEmailService.SendEmailAsync(sendRequest);
                bool status = !string.IsNullOrEmpty(response?.MessageId) ? true : false;
                logger.LogInformation($"MessageID: {response?.MessageId}, Status: {status}");

                return status;
            }
            catch (Exception ex)
            {
                logger.LogError($"The email was not sent.");
                logger.LogError($"Error message: {ex.Message}");
                return false;
            }
        }

        public async Task<bool> SendSMTPEmail(SendEmailSMTPModel messageContent)
        {
            try
            {
                using (MailMessage mailMessage = new MailMessage())
                {
                    mailMessage.From = new MailAddress(settings.EmailInfo.UserName);
                    mailMessage.Subject = messageContent.Subject;
                    mailMessage.Body = messageContent.HtmlBody;
                    mailMessage.IsBodyHtml = true;
                    logger.LogInformation("Send SMTP Email - Start");
                    string[] multi = messageContent.To.Split(',');
                    foreach (string mutiEmail in multi)
                    {
                        mailMessage.To.Add(new MailAddress(mutiEmail));
                    }

                    if (!string.IsNullOrEmpty(messageContent.BccmailIds))
                    {
                        string[] multIBCC = messageContent.BccmailIds.Split(',');
                        foreach (string mutiEmailBCC in multIBCC)
                        {
                            mailMessage.Bcc.Add(new MailAddress(mutiEmailBCC));
                        }
                    }

                    SmtpClient smtp = new SmtpClient(); // Todo Jeya
                    smtp.Host = settings.EmailInfo.Host;
                    smtp.EnableSsl = Convert.ToBoolean(settings.EmailInfo.EnableSsl);

                    System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential(); // Todo jeya
                    NetworkCred.UserName = settings.EmailInfo.SmtpUserName;
                    NetworkCred.Password = settings.EmailInfo.SmtpPassword;

                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = int.Parse(settings.EmailInfo.Port);
                    smtp.Send(mailMessage);
                    logger.LogInformation("Send Email - End");

                    return true;
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"{ex.Message}");
                //await SendSnsNotification(ToEmailID, subject, ex);
                //objDBL.LOG(ex.Message, $"{URL}/SendHtmlFormattedEmail", "", 1);
                return false;
            }
        }
        #endregion

        #region PrivateMethods

        #endregion
    }
}
