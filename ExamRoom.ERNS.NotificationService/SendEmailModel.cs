﻿using System;
using System.Collections.Generic;

namespace ExamRoom.ERNS.NotificationService
{
    public class SendEmailModel
    {
        public string SenderAddress { get; set; }
        public string  Subject { get; set; }
        public string  TextBody { get; set; }
        public string  HtmlBody { get; set; }
        public List<string>  ToAddress { get; set; }
        public string To { get; set; }
        public List<string> BccmailIds { get; set; }
        public List<string> CcmailIds { get; set; }
    }
    public class SendEmailSMTPModel
    {
        public string SenderAddress { get; set; }
        public string Subject { get; set; }
        public string TextBody { get; set; }
        public string HtmlBody { get; set; }
        public List<string> ToAddress { get; set; }
        public string To { get; set; }
        public string BccmailIds { get; set; }
    }
}
